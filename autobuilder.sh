#!/bin/sh

REPO_DIR=~/repositories
PKG_DIR=~/repositories/pkgs
ACTIVE_DISTS="f39 f38 epel9 epel8"

while getopts ":t:p:" opt; do
    case $opt in
        t)
            BUILDTYPE=$OPTARG
            ;;
        p)
            PACKAGE=$OPTARG
            ;;
    esac
done

# fetch version and commit info
fetch_version_and_commit ()
{
    pushd $REPO_DIR/$PACKAGE
    git fetch origin
    LATEST_TAG=$(git tag --sort=creatordate | tail -1)
    echo $LATEST_TAG | grep "rc\|RC" > /dev/null
    if [[ $? -eq 0 ]]; then
        echo "Latest tag is RC. Exiting..."
        exit 0
    else
        echo "Latest tag is not RC. Continuing..."
    fi
    git checkout $LATEST_TAG
    if [[ $? -ne 0 ]]; then
        echo "Error checking out Latest Tag: $LATEST_TAG. Investigate!!!"
        exit 1
    fi
    LATEST_VERSION=$(echo $LATEST_TAG | sed -e 's/^v//' -e 's/-/~/')
    echo $LATEST_VERSION | grep -v -- "[+_]" > /dev/null
    if [[ $? -ne 0 ]]; then
        echo "New Version string $LATEST_VERSION not good. Investigate!!!"
        exit 1
    fi
    echo "New Version string $LATEST_VERSION good to go. Continuing..."
    popd
}

compare_versions ()
{
    pushd $PKG_DIR/$PACKAGE
    git fetch origin
    if [[ $? -ne 0 ]]; then
        echo "Error fetching from remote. Investigate..."
        exit 1
    fi
    git checkout --track origin/rawhide -b rawhide
    if [[ $? -ne 0 ]]; then
        echo "Checking out rawhide failed. Investigate..."
        exit 1
    fi
    CURRENT_TAG=$(grep '^%global built_tag ' $PACKAGE.spec | sed -e 's/.*tag //')
    CURRENT_VERSION=$(echo $CURRENT_TAG | sed -e 's/^v//' -e 's/-/~/')
    rpmdev-vercmp $CURRENT_VERSION $LATEST_VERSION
    VERCMP=$?
    if [[ $VERCMP -eq 12 ]]; then
	    echo "Updating Version field in $PACKAGE spec file..."
	    sed -i "s/^%global built_tag .*/%global built_tag $LATEST_TAG/" $PACKAGE.spec
    elif [[ $VERCMP -eq 0 || $VERCMP -eq 11 ]]; then
        echo "Current version greater than or equal to latest published tag version. Exiting..."
        exit 0
    else
        echo "rpmdev-vercmp exited weirdly. Investigate..."
        exit 1
    fi
    popd
}

#FIXME
bundling_stuff ()
{
    # LICENSES
    find . -name *LICENSE* -exec licensecheck {} \;

    # golang bundled stuff
    awk '{print "Provides: bundled(golang("$1")) = "$2}' go.mod | sort | uniq | sed -e 's/-/_/g' -e '/bundled(golang())/d' -e '/bundled(golang(go\|module\|replace\|require))/d'

    # rust bundled stuff
    cargo tree --prefix none | awk '{print "Provides: bundled(crate("$1")) = "$2}' | sort | uniq
}

handle_non_vendored_packages ()
{
    pushd $REPO_DIR/$PACKAGE
    git clean -dfx
    git checkout $LATEST_TAG
    if [[ $? -ne 0 ]]; then
        echo "Error checking out Latest Tag: $LATEST_TAG. Investigate!!!"
        exit 1
    fi
    if [[ $PACKAGE == "stargz-snapshotter" ]]; then
        pushd cmd
    fi
    echo "Running go mod vendor..."
    go mod vendor
    echo "Adding vendor files to git..."
    git add vendor/*
    if [[ $? -ne 0 ]]; then
        echo "Error adding vendor files to git. Investigate!!!"
        exit 1
    fi
    if [[ $PACKAGE = "stargz-snapshotter" ]]; then
        popd
    fi
    git commit -am 'add vendor/* files for tarball generation'
    if [[ $? -ne 0 ]]; then
        echo "Error committing vendored files to git. Investigate!!!"
        exit 1
    fi
    echo "Creating tarball for $PACKAGE..."
    git archive --prefix=$PACKAGE-$LATEST_VERSION/ -o $LATEST_TAG-vendor.tar.gz HEAD
    if [[ $? -ne 0 ]]; then
        echo "Error creating $PACKAGE tarball. Investigate!!!"
        exit 1
    fi
    echo "Moving $LATEST_TAG-vendor.tar.gz to $PKG_DIR/$PACKAGE..."
    mv $LATEST_TAG-vendor.tar.gz $PKG_DIR/$PACKAGE/.
    popd
}

# update dist-git
commit_to_dist_git ()
{
    pushd $PKG_DIR/$PACKAGE
    echo "Removing any untracked files including stale tarballs..."
    git clean -dfx
    if [[ $? -ne 0 ]]; then
        echo "Error cleaning non-git files. Investigate..."
    fi
    echo "Removing any stale BUILD directories..."
    rm -rf BUILD*
    if [[ $PACKAGE == "pack" || $PACKAGE == "stargz-snapshotter" ]]; then
        handle_non_vendored_packages
    else
        spectool -g $PACKAGE.spec
    fi
    echo $FEDORA_KRB_PASSWORD | kinit rhcontainerbot@FEDORAPROJECT.ORG
    if [[ $? -ne 0 ]]; then
        echo "Kerberos authentication failed. Investigate!!!"
        exit 1
    fi
    fedpkg new-sources *.tar.*z
    if [[ $? -ne 0 ]]; then
        echo "Error uploading source tarball to lookaside cache. Investigate..."
        exit 1
    fi
    git commit -a -m "auto bump to $LATEST_TAG"
    popd
}

# push and build
push_and_build ()
{
    pushd $PKG_DIR/$PACKAGE
    git push -u origin rawhide
    if [[ $? -ne 0 ]]; then
        echo "git push rawhide to dist-git FAIL!!!"
        exit 1
    fi
    fedpkg build --fail-fast
    if [[ $? -ne 0 ]]; then
        echo "Something went wrong with fedpkg build. Investigate!!!"
        exit 1
    fi
    for DIST_GIT_TAG in $ACTIVE_DISTS; do
        git checkout --track origin/$DIST_GIT_TAG -b $DIST_GIT_TAG
        if [[ $? -ne 0 ]]; then
            echo "Branch does not exist. Continuing..."
            continue
        fi

        echo "Trying a git merge from rawhide into $DIST_GIT_TAG..."
        git merge --no-commit --ff-only rawhide
        if [[ $? -eq 0 ]]; then
            git reset --hard
            if [[ $? -ne 0 ]]; then
                echo "Error resetting merge --no-commit. Investigate!!!"
                exit 1
            fi
            echo "Merging from rawhide to $DIST_GIT_TAG..."
            git merge --ff-only rawhide
        else
            echo "git merge will not work, trying cherry-pick..."
            git cherry-pick --no-commit -x rawhide
            if [[ $? -eq 0 ]]; then
                git reset --hard
                if [[ $? -ne 0 ]]; then
                    echo "Error resetting cherry-pick --no-commit. Investigate!!!"
                    exit 1
                fi
                echo "Cherry-picking latest commit from rawhide into $DIST_GIT_TAG..."
                git cherry-pick -x rawhide
            else
                echo "Neither merge nor cherry-pick works for $PACKAGE on $DIST_GIT_TAG. Investigate!!!"
                exit 1
            fi
        fi
        git push -u origin $DIST_GIT_TAG
        if [[ $? -ne 0 ]]; then
            echo "Failed to push $DIST_GIT_TAG branch. Investigate!!!"
            exit 1
        fi
        fedpkg build --nowait
        if [[ $? -ne 0 ]]; then
            echo "fedpkg build FAILED for $DIST_GIT_TAG branch. Investigate!!!"
            exit 1
        fi
    
    done
    popd
}

cd ~/.ssh
openssl enc -aes-256-cbc -pbkdf2 -d -in id_ed25519.enc -out id_ed25519 -pass pass:$DECRYPTION_PASSPHRASE
chmod 600 id_ed25519
cd ~/repositories/pkg-builder
git fetch --all
if [[ $? -ne 0 ]]; then
    echo "Error fetching latest sources from pkg-builder remote. Investigate!!!"
    exit 1
fi
git checkout fedora
if [[ $? -ne 0 ]]; then
    echo "Error checking out fedora branch. Investigate!!!"
    exit 1
fi
git pull
if [[ $? -ne 0 ]]; then
    echo "Error pulling latest fedora branch in pkg-builder repo. Investigate!!!"
    exit 1
fi

fetch_version_and_commit
compare_versions
commit_to_dist_git

if [[ $BUILDTYPE == "tagged" ]]; then
   push_and_build
fi
