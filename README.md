# Fedora Packaging HOWTO

Create an account on the Fedora Account System https://accounts.fedoraproject.org
Once you have the Fedora account created, ping Lokesh Mandvekar (@lsm5). He will file a sponsorship
ticket to get you inclued in the packager group.

Sample ticket filed: https://pagure.io/packager-sponsors/issue/442


## Install prerequisites 
```
$ sudo dnf -y install fedora-packager rpmdevtools krb5-workstation
```

## Create ~/.rpmmacros file that looks like so:
```
%packager $YOUR\_NAME <$YOUR_EMAIL>
%_topdir %(echo $(pwd))

%_sourcedir     %{_topdir}
%_specdir       %{_sourcedir}
%_rpmdir        %{_topdir}/RPMS
%_srcrpmdir     %{_topdir}/SRPMS
%_builddir      %{_topdir}/BUILD
%_tmppath       %(echo ${TMPDIR:-/var/tmp})
%_buildroot     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
     
%_smp_mflags %( \
    [ -z "$RPM_BUILD_NCPUS" ] \\\
        && RPM_BUILD_NCPUS="`/usr/bin/nproc 2>/dev/null || \\\
                             /usr/bin/getconf _NPROCESSORS_ONLN`"; \\\
    if [ "$RPM_BUILD_NCPUS" -gt 16 ]; then \\\
        echo "-j16"; \\\
    elif [ "$RPM_BUILD_NCPUS" -gt 3 ]; then \\\
        echo "-j$RPM_BUILD_NCPUS"; \\\
    else \\\
        echo "-j3"; \\\
    fi )

%__arch_install_post \
    [ "%{buildarch}" = "noarch" ] || QA_CHECK_RPATHS=0 ; \
    case "${QA_CHECK_RPATHS:-}" in [1yY]*) /usr/lib/rpm/check-rpaths ;; esac \
    /usr/lib/rpm/check-buildroot
```

This file will place all rpms, srpms and build dirs inside the current dir as opposed to a single system wide tree, which can be really inconvenient to manage.

## Check if your packager name and email are set correctly. This uses values set in ~/.rpmmacros. You can either hardcode it or use values from your ~/.gitconfig as mentioned above.
```
# This should show your name and email
$ rpm --eval %packager
Lokesh Mandvekar <lsm5@fedoraproject.org>
```

## Get a Kerberos ticket from Fedora
```
# Domain name should be all uppercase
# FAS\_USERNAME is your Fedora account username
$ kinit $FAS_USERNAME@FEDORAPROJECT.ORG
Password for $FAS_USERNAME@FEDORAPROJECT.ORG:
$
```
 Login to https://src.fedoraproject.org with your FAS account credentials, after which you will be visible in the packager list. This step will let current package maintainers add you as maintainers to the rpm packages.


## Quick walkthrough for building rpms (using podman as an example)

The package maintenance process mostly uses the `fedpkg` tool. It uses many git / koji / bodhi operations under the hood.
```
# Clone the package source
$ fedpkg clone podman
$ cd podman
$ ls
gating.yaml  podman.spec  sources  tests
```


The  *.spec file is the main file you deal with for packaging. It will mention package metadata,dependencies, along with links to fetch the upstream source, and also the build and install rules. See: https://rpm-packaging-guide.github.io/#what-is-a-spec-file for more details.
The sources file mentions the file name for the upstream source tarball used for building the package along with checksum for verification.

## Check the branches in the repository. The “master” branch is used for building the rawhide package. The f33 branch for Fedora 33 and you get the drift.
```
$ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/centos-release
  remotes/origin/f27
  remotes/origin/f28
  remotes/origin/f29
  remotes/origin/f30
  remotes/origin/f31
  remotes/origin/f32
  remotes/origin/f33
  remotes/origin/master
```


## Download the source tarball, untar it and apply any patches using fedpkg
```
$ fedpkg prep
Downloading dnsname-8a6a8a4.tar.gz
######################################################################## 100.0%
Downloading podman-7c12967.tar.gz
######################################################################## 100.0%

setting SOURCE_DATE_EPOCH=1601683200
Executing(%prep): /bin/sh -e /var/tmp/rpm-tmp.EydCAb
+ umask 022
+ cd /home/lsm5/test/podman
+ cd /home/lsm5/test/podman
+ rm -rf podman-7c12967257742063206c05f0baec517bc08cbeb6
+ /usr/bin/gzip -dc /home/lsm5/test/podman/podman-7c12967.tar.gz
+ /usr/bin/tar -xof -
+ STATUS=0
+ '[' 0 -ne 0 ']'
+ cd podman-7c12967257742063206c05f0baec517bc08cbeb6
+ /usr/bin/chmod -Rf a+rX,u+w,g-w,o-w .
+ /usr/bin/git init -q
+ /usr/bin/git config user.name rpm-build
+ /usr/bin/git config user.email '<rpm-build>'
+ /usr/bin/git config gc.auto 0
+ /usr/bin/git add --force .
+ /usr/bin/git commit -q --allow-empty -a --author 'rpm-build <rpm-build>' -m 'podman-2.2.0 base'
+ tar zxf /home/lsm5/test/podman/dnsname-8a6a8a4.tar.gz
+ RPM_EC=0
++ jobs -p
+ exit 0
```

## Install build dependencies
```
dnf builddep -y podman.spec
```

## Build the rpm
```
$ rpmbuild -ba podman.spec
setting SOURCE_DATE_EPOCH=1601683200
Executing(%prep): /bin/sh -e /var/tmp/rpm-tmp.17IHEJ
+ umask 022
+ cd /home/lsm5/test/podman/BUILD
+ cd /home/lsm5/test/podman/BUILD
+ rm -rf podman-7c12967257742063206c05f0baec517bc08cbeb6
+ /usr/bin/gzip -dc /home/lsm5/test/podman/podman-7c12967.tar.gz
+ /usr/bin/tar -xof -
+ STATUS=0
+ '[' 0 -ne 0 ']'
+ cd podman-7c12967257742063206c05f0baec517bc08cbeb6
+ /usr/bin/chmod -Rf a+rX,u+w,g-w,o-w .
+ /usr/bin/git init -q
+ /usr/bin/git config user.name rpm-build
+ /usr/bin/git config user.email '<rpm-build>'
+ /usr/bin/git config gc.auto 0
+ /usr/bin/git add --force .
...... MANY MANY MANY LINES LATER .....
Wrote: /home/lsm5/test/podman/SRPMS/podman-2.2.0-0.19.dev.git7c12967.fc33.src.rpm
Wrote: /home/lsm5/test/podman/RPMS/x86_64/podman-tests-2.2.0-0.19.dev.git7c12967.fc33.x86_64.rpm
Wrote: /home/lsm5/test/podman/RPMS/noarch/podman-docker-2.2.0-0.19.dev.git7c12967.fc33.noarch.rpm
Wrote: /home/lsm5/test/podman/RPMS/x86_64/podman-plugins-2.2.0-0.19.dev.git7c12967.fc33.x86_64.rpm
Wrote: /home/lsm5/test/podman/RPMS/x86_64/podman-plugins-debuginfo-2.2.0-0.19.dev.git7c12967.fc33.x86_64.rpm
Wrote: /home/lsm5/test/podman/RPMS/x86_64/podman-debugsource-2.2.0-0.19.dev.git7c12967.fc33.x86_64.rpm
Wrote: /home/lsm5/test/podman/RPMS/x86_64/podman-remote-2.2.0-0.19.dev.git7c12967.fc33.x86_64.rpm
Wrote: /home/lsm5/test/podman/RPMS/x86_64/podman-remote-debuginfo-2.2.0-0.19.dev.git7c12967.fc33.x86_64.rpm
Wrote: /home/lsm5/test/podman/RPMS/x86_64/podman-2.2.0-0.19.dev.git7c12967.fc33.x86_64.rpm
Wrote: /home/lsm5/test/podman/RPMS/x86_64/podman-debuginfo-2.2.0-0.19.dev.git7c12967.fc33.x86_64.rpm
Executing(%clean): /bin/sh -e /var/tmp/rpm-tmp.qiP6ZC
+ umask 022
+ cd /home/lsm5/test/podman/BUILD
+ cd podman-7c12967257742063206c05f0baec517bc08cbeb6
+ /usr/bin/rm -rf /home/lsm5/test/podman/BUILDROOT/podman-2.2.0-0.19.dev.git7c12967.fc33.x86_64
+ RPM_EC=0
++ jobs -p
+ exit 0
```




## Example 1: Rebuilding the package with a new release tag
Let’s assume the case of a simple rebuild of the existing sources on Fedora rawhide. The procedure to update the Fedora package is as documented below.

The rebuild needs the Release tag in the spec file to be bumped. This can be accomplished easily with a single command
```
$ rpmdev-bumpspec -Dc ‘rebuild’ podman.spec
```

# Check the diff if the command worked. Same as git diff
 ```
$fedpkg diff
diff --git a/podman.spec b/podman.spec
index c9df28c..c5ded3c 100644
--- a/podman.spec
+++ b/podman.spec
@@ -57,7 +57,7 @@ Version: 2.2.0
 # N.foo if released, 0.N.foo if unreleased
 # Rawhide almost always ships unreleased builds,
 # so release tag should be of the form 0.N.foo
-Release: 0.19.dev.git%{shortcommit0}%{?dist}
+Release: 0.20.dev.git%{shortcommit0}%{?dist}
 Summary: Manage Pods, Containers and Container Images
 License: ASL 2.0
 URL: https://%{name}.io/
@@ -614,6 +614,9 @@ exit 0

 # rhcontainerbot account currently managed by lsm5
 %changelog
+* Sat Oct  3 2020 Lokesh Mandvekar <lsm5@fedoraproject.org> - 2:2.2.0-0.20.dev.git7c12967
+- rebuild
+
 * Sat Oct  3 2020 RH Container Bot <rhcontainerbot@fedoraproject.org> - 2:2.2.0-0.19.dev.git7c12967
 - autobuilt 7c12967
```


## Commit the changes using fedpkg commit. Same as git commit
```
$ fedpkg commit
# Add and save commit message
```

## Push changes using fedpkg push. Same as git push
```
$ fedpkg push 
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 371 bytes | 371.00 KiB/s, done.
Total 3 (delta 2), reused 0 (delta 0), pack-reused 0
remote: Emitting a message to the fedora-messaging message bus.
remote: Traceback (most recent call last):
remote:   File "/usr/share/git-core/post-receive-fedora-messaging", line 153, in <module>
remote:     commits = [_build_commit(rev) for rev in revs]
remote:   File "/usr/share/git-core/post-receive-fedora-messaging", line 153, in <listcomp>
remote:     commits = [_build_commit(rev) for rev in revs]
remote:   File "/usr/share/git-core/post-receive-fedora-messaging", line 54, in revs_between
remote:     for line in stdout.decode('utf-8').strip().split('\n'):
remote: AttributeError: 'str' object has no attribute 'decode'
remote: Sending to redis to log activity and send commit notification emails
remote: * Publishing information for 1 commits
remote:   - to fedora-message
To ssh://pkgs.fedoraproject.org/rpms/podman
   961c224..6fd4f9f  master -> master
$
```

## Build the new package on Koji
```
$ fedpkg build
Found a gating.yaml file in the repo and it is properly configured
error: %changelog not in descending chronological order

Building podman-2.2.0-0.20.dev.git7c12967.fc34 for rawhide
Created task: 52711190
Task info: https://koji.fedoraproject.org/koji/taskinfo?taskID=52711190
Watching tasks (this may be safely interrupted)...
52711190 build (rawhide, /rpms/podman.git:6fd4f9f2dfa868def76149767d27a9286db53a69): free
52711190 build (rawhide, /rpms/podman.git:6fd4f9f2dfa868def76149767d27a9286db53a69): free -> open (buildvm-x86-25.iad2.fedoraproject.org)
  52711233 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, armv7hl): open (buildvm-a32-05.iad2.fedoraproject.org)
  52711237 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, ppc64le): open (buildvm-ppc64le-04.iad2.fedoraproject.org)
  52711238 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, s390x): open (buildvm-s390x-18.s390.fedoraproject.org)
  52711234 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, i686): open (buildvm-x86-29.iad2.fedoraproject.org)
  52711191 buildSRPMFromSCM (/rpms/podman.git:6fd4f9f2dfa868def76149767d27a9286db53a69): closed
  52711236 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, aarch64): open (buildvm-a64-30.iad2.fedoraproject.org)
  52711235 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, x86_64): open (buildvm-x86-30.iad2.fedoraproject.org)
  52711234 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, i686): open (buildvm-x86-29.iad2.fedoraproject.org) -> closed
  0 free  6 open  2 done  0 failed
  52711235 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, x86_64): open (buildvm-x86-30.iad2.fedoraproject.org) -> closed
  0 free  5 open  3 done  0 failed
  52711237 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, ppc64le): open (buildvm-ppc64le-04.iad2.fedoraproject.org) -> closed
  0 free  4 open  4 done  0 failed
  52711238 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, s390x): open (buildvm-s390x-18.s390.fedoraproject.org) -> closed
  0 free  3 open  5 done  0 failed
  52711233 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, armv7hl): open (buildvm-a32-05.iad2.fedoraproject.org) -> closed
  0 free  2 open  6 done  0 failed
  52711236 buildArch (podman-2.2.0-0.20.dev.git7c12967.fc34.src.rpm, aarch64): open (buildvm-a64-30.iad2.fedoraproject.org) -> closed
  0 free  1 open  7 done  0 failed
52711190 build (rawhide, /rpms/podman.git:6fd4f9f2dfa868def76149767d27a9286db53a69): open (buildvm-x86-25.iad2.fedoraproject.org) -> closed
  0 free  0 open  8 done  0 failed
  52711446 tagBuild (noarch): closed

52711190 build (rawhide, /rpms/podman.git:6fd4f9f2dfa868def76149767d27a9286db53a69) completed successfully
```



## Example 2: Build a new upstream version for Fedora 33
DO NOT TRY THIS RIGHT AWAY. USE IT ONLY FOR REFERENCE IN CASE SOMETHING BREAKS WITH THE AUTOBUILDER JOB.

First switch to the f33 branch using fedpkg switch-branch. Same as git checkout
```
$ fedpkg switch-branch f33
```

## Use "git branch" to check if you're on the f33 branch


Building a new upstream version (say v2.2.0) would involve bumping the Version field in the spec file, along with the Release field. This can be done with rpmdev-bumpspec as well.
```
$ rpmdev-bumpspec -Dn '2.2.0' podman.spec
```

## Check if the version and release field got bumped along with a new changelog entry with 'git diff' or 'fedpkg diff'

## Fetch the new upstream tarball
```
$ spectool -g podman.spec
```

Build the rpm as in step 5 above. The rpms will be placed in the RPMS/ directory. Feel free to install them using `dnf install` and test if they work fine.

Use fedpkg to upload the source tarball to the lookaside cache. This step will also update the sources in the repo. The sources file should now mention the latest upstream tarball name.
```
$ fedpkg new-sources v2.2.0.tar.gz
```


Commit and push changes and build the package on Koji as mentioned above.

## Bodhi Updates

The non-rawhide branches need an additional step where you create a “Bodhi Update”. This can be done
using the “fedpkg update” command. This will open up a document using whatever editor your
$EDITOR environment variable points to. You need to choose the Update type
(newpackage, enhancement, bugfix or security) and include bugs or update notes if relevant.
Once you save it, it will submit an update and send you details of the bodhi update.

Alternatively, you can head to https://bodhi.fedoraproject.org/ and hit
`New Update` and fill out the form to submit a new update.



# Build Automation on GitLab

The CI job handles the entire process of checking new upstream releases, bumping and building
them on koji. These run on an hourly basis, so you’re never too far from the next update.
You can also trigger them by going to the CI/CD -> Schedules menu item and hitting the
Play button against any pipeline.

The file .gitlab-ci.yml  mentions the container image used for the build jobs as well as the
script used.

The file autobuilder.sh handles all the rpmbuild and fedpkg steps that were mentioned above.
